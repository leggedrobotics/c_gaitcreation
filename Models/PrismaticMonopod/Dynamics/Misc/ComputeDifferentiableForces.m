% *************************************************************************
%
% function [f_diff, F_l, T_alpha] = ComputeDifferentiableForces(y, u, p)
% 
% This MATLAB function computes the vector of differentiable forces for
% a prismatic monopod in 2D.  I.e. the sum of gravitational forces,
% coriolis forces, and the forces in the actuator springs. The model's
% current continuous states and the  model parameters are provided by the
% calling routine to which the differentiable force vector is returned.   
% 
%
% Input:  - A vector of continuous states 'y' 
%         - A vector of excitation states 'u' 
%         - A vector of model system parameters 'p'
%
% Output: - The differentiable force-vector 'f_diff'
%         - The leg actuator force 'F_l'
%         - The hip actuator torque 'T_alpha'
%
% Created by C. David Remy on 03/14/2011
% MATLAB 2010a
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also HYBRIDDYNAMICS, FLOWMAP, JUMPSET,
%            CONTSTATEDEFINITION, SYSTPARAMDEFINITION,
%            EXCTSTATEDEFINITION. 
%
function [f_diff, F_l, T_alpha] =  ComputeDifferentiableForces(y, u, p)
    
	% Get a mapping for the state and parameter vectors.
    % Keep the index-structs in memory to speed up processing
    persistent contStateIndices systParamIndices exctStateIndices
    if isempty(contStateIndices) || isempty(systParamIndices) || isempty(exctStateIndices)
        [~, ~, contStateIndices] = ContStateDefinition();
        [~, ~, systParamIndices] = SystParamDefinition();
        [~, ~, exctStateIndices] = ExctStateDefinition();
    end
    
    % Compute the viscous damping coefficient of the spring, according to the desired
    % damping ratio:
    j_leg   = p(systParamIndices.j3) + (p(systParamIndices.l_0) - p(systParamIndices.l3))^2*p(systParamIndices.m3) + p(systParamIndices.j2) + p(systParamIndices.l2)^2*p(systParamIndices.m2); % total leg inertia wrt the hip
    balpha  = p(systParamIndices.balphaRat)*2*sqrt(p(systParamIndices.kalpha)*j_leg); 
    bl      = p(systParamIndices.blRat)*2*sqrt(p(systParamIndices.kl)*p(systParamIndices.m3)); 
    
    % Compute spring and damping forces:
    F_l     = p(systParamIndices.kl)*(p(systParamIndices.l_0) + u(exctStateIndices.ul)  - y(contStateIndices.l)) + ...
              bl*(                                            + u(exctStateIndices.dul) - y(contStateIndices.dl));
    T_alpha = p(systParamIndices.kalpha)*(p(systParamIndices.alpha_0) + u(exctStateIndices.ualpha)  - y(contStateIndices.alpha)) + ...
              balpha*(                                                + u(exctStateIndices.dualpha) - y(contStateIndices.dalpha));
    
    % Actuator forces:
    f_act = [0; 0; 0; T_alpha; F_l];
    % Graviational and coriolis forces:
    f_cg = F_CoriGravWrapper(y,p);
    % All differentiable forces:
    f_diff = f_cg + f_act;
end