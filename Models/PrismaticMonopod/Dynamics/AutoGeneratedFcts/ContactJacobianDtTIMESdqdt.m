function dJdtTIMESdqdt = ContactJacobianDtTIMESdqdt(x,y,phi,alpha,l,dx,dy,dphi,dalpha,dl,l2,l3,rFoot,g,m1,m2,m3,j1,j2,j3)
%CONTACTJACOBIANDTTIMESDQDT
%    DJDTTIMESDQDT = CONTACTJACOBIANDTTIMESDQDT(X,Y,PHI,ALPHA,L,DX,DY,DPHI,DALPHA,DL,L2,L3,RFOOT,G,M1,M2,M3,J1,J2,J3)

%    This function was generated by the Symbolic Math Toolbox version 5.4.
%    10-Apr-2011 10:50:09

dJdtTIMESdqdt = [-(dalpha+dphi).*(dl.*cos(alpha+phi).*-2.0+dalpha.*l.*sin(alpha+phi)+dphi.*l.*sin(alpha+phi));(dalpha+dphi).*(dl.*sin(alpha+phi).*2.0+dalpha.*l.*cos(alpha+phi)+dphi.*l.*cos(alpha+phi))];
